#!/bin/sh

mountable=$(lsblk -lp | grep "part $" | awk '{print $1, "[ "$4" ]"}')
chosen=$(echo "$mountable" | dmenu -i -p "Mount part:" -fn 'Ubuntu -10' -nb '#000000' -sb '#1a7a3f' | awk '{print $1}')
udisksctl mount -b "$chosen" && notify-send -i ~/.config/scripts/flash.svg "Disk mounted"  

