#!/bin/sh

mountable=$(lsblk -lp |grep "/run/media" | awk '{print $1, " ["$4"]"}')
chosen=$(echo "$mountable"|dmenu -i -p "Unmount part:" -fn 'Ubuntu -10' -nb '#000000' -sb '#1a7a3f' | awk '{print $1}')
udisksctl unmount -b "$chosen" && notify-send -i ~/.config/scripts/disk.svg "Disk unmounted" && exit 0 

